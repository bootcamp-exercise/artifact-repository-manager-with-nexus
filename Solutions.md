You and other teams in 2 different projects in your company see that they have many different small projects, including the NodeJS application you built in the previous step, the java-gradle helper application and so on. You discuss and decide it would be a good idea to be able to keep all these app artifacts in 1 place, where each team can keep their app artifacts and can access them when they need.
So they ask you to setup Nexus in the company and create repositories for 2 different projects.

<details>
<summary>Exercise 1: Install Nexus on a server </summary>
 <br />

```sh
#Login to the droplet server and Install java version 8 on droplet server

root@{server-ip}

apt install openjdk-8-jre-headless


cd /opt

wget  https://download.sonatype.com/nexus/3/nexus-3.59.0-01-unix.tar.gz
untar the file
tar -xzvf nexus-3.59.0-01-unix.tar.gz

#create user nexus
adduser nexus
chown -R nexus:nexus nexus-3.59.0-01
chown -R nexus:nexus sonatype-work

#add user as nexus in this file
vim nexus-3.59.0-01/bin/nexus.rc

#login as a nexus user and start nexus
su - nexus
/opt/nexus-3.59.0-01/bin/nexus start

#check the port on which nexus is running
ps -aux
netstat -lnpt


#open firewall rules for nexus port in the droplet

#to access nexus from browser
{ip-address}:{potnumber} in the URl to open nexus

```

</details>


<details>
<summary>Exercise 2: Create npm hosted repositor </summary>
 <br />

```sh
#login to nexus, create a password

#go to repositories and create npm-hosted repository



```

</details>



<details>
<summary>Exercise 3: Create user for team 1 </summary>
 <br />

```sh

#Create user by clicking on create user and assign the role for the user to access the npm repository which we created.



```

</details>



<details>
<summary>Exercise 4: Build and publish npm tar </summary>
 <br />

```sh

#add these lines in package.json

"publishConfig": {
"registry": "http://{nexus-repo-ipaddr}:{port-number}/repository/soni-npm-hosted-repo/"
}


#add npm realm in nexus
go to settings>realms>add npm bearer token realm


#publish the tar file
npm publish
provide username and password

npm package will now be available in the npm repo on nexus.

```

</details>



<details>
<summary>Exercise 5: Create maven hosted repository </summary>
 <br />

```sh

#create a new maven hosted repo in nexus

```

</details>

<details>
<summary>Exercise 6: Create user for team 2 </summary>
 <br />

```sh

#Create user for this maven repo and assign role to access the repo

```

</details>
<details>
<summary>Exercise 7: Build and publish jar file </summary>
 <br />

```sh

# add these lines  in build.gradle file 
publishing {
    publications {
        maven(MavenPublication) {
            artifact("build/libs/my-app-$version"+".jar"){
                extension 'jar'
            }
        }
    }

    repositories {
        maven {
            name 'maven'
            url "http://{nexus-ip-address:portnumber}repository/maven-snapshots/"
            allowInsecureProtocol = true
            credentials {
                username project.repoUser
                password project.repoPassword
            }
        }
    }
}
#create gradle.properties file and add reposuer and password, 
# add project name in settings.gradle file.
#this will publish the jar file in nexus repo(maven -snapshots)
gradle build
gradle publish


```

</details>
<details>
<summary>Exercise 8: Download from Nexus and start application </summary>
 <br />

```sh

#create a new user that can access both npm and maven repositories

# fetch download URL with curl
curl -L -u {user}:{password} -X GET 'http://{nexus-ip}:8081/service/rest/v1/components?repository={node-repo}&sort=version'


tar -xzvf bootcamp-node-project-1.0.0.tgz

#cd into package folder
cd package

#install dependencies
npm install

#run the app
node server.js

#open the app on the browser
localhost:{portnumber}
```

</details>
<details>
<summary>Exercise 9: Automate </summary>
 <br />

```sh
#automating the task by fetching the latest version from npm repository. 
# Untar it and run on the server 

# save the artifact details in a json file
curl _L -u {user}:{password} -X GET 'http://{nexus-ip}:8081/service/rest/v1/components?repository={node-repo}&sort=version' | jq "." > artifact.json

# grab the download url from the saved artifact details using 'jq' json processor tool
artifactDownloadUrl=$(jq '.items[].assets[].downloadUrl' artifact.json --raw-output)

# fetch the artifact with the extracted download url using 'wget' tool
wget --http-user={user} --http-password={password} $artifactDownloadUrl

```

</details>



















